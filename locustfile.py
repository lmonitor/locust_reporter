# coding: utf-8
from locust import HttpLocust, TaskSet, task, events, runners
from influxdb import InfluxDBClient


monitor = InfluxDBClient('localhost', '8086', database='loaddb')


class TaskTest(TaskSet):
    @task
    def open_root_page(self):
        resp = self.client.get("/")
        points = [{
            'measurement': 'responses',
            'tags': {'host': self.locust.host},
            'fields': {
                "host": self.locust.host,
                "code": resp.status_code,
            },
        }]
        monitor.write_points(points)


class MyLocust(HttpLocust):
    task_set = TaskTest
    host = "http://192.168.33.10:5999"

    def __init__(self):
        super(MyLocust, self).__init__()
        events.request_success += self.request_success_handler

    def request_success_handler(
            self,
            request_type, name, response_time, response_length
    ):
        points = [{
            'measurement': 'requests',
            'tags': {'host': self.host},
            'fields': {
                "host": self.host,
                "response_time": float(response_time),
                "user_count": runners.locust_runner.user_count,
            },
        }]
        monitor.write_points(points)
