# coding: utf-8
from locust import HttpLocust, TaskSet, task


class TaskTest(TaskSet):

    @task
    def open_root_page(self):
        self.client.get("/")


class MyLocust(HttpLocust):
    task_set = TaskTest
    host = "http://127.0.0.1:5999"
